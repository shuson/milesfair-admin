<?php

namespace milesfair\Http\Controllers\Auth;

use Illuminate\Http\Request;
use milesfair\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use milesfair\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['changePassword', 'loadChangePasswordView']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function loadChangePasswordView()
    {
        return view("auth.security");
    }

    /**
     * Change password
     */
    public function changePassword(Request $request){
        $this->validate($request, [
            'new_password'  => "required|min:6",
            'password_confirmation' => "same:new_password"
        ]);

        $old_password = Input::get('old_password');
        $user = User::find(Auth::user()->id);

        if(Hash::check($old_password, $user->getAuthPassword()) or !$user->getAuthPassword()){
            $user->password = Hash::make(Input::get('new_password'));
            $user->save();

            return Redirect::back()->withInput()->with([
                "success" => true,
            ]);
        }

        return Redirect::back()->withErrors((["old password is invalid"]));
    }
}
