<?php
/**
 * Created by PhpStorm.
 * User: shuson
 * Date: 14/12/15
 * Time: 11:22 PM
 */

namespace milesfair\Http\Controllers;

use milesfair\Http\Requests;
use milesfair\Models\Transaction;
use Carbon\Carbon;
use milesfair\Models\User;

class DashboardController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Display a new transactions by day.
     *
     * @return \Illuminate\Http\Response
     */
    public function countByToday()
    {
        //
        $countTransactions = Transaction::whereBetween('created_at', array(Carbon::yesterday(), Carbon::now()->addDay()))->count();
        $countUsers = User::whereBetween('created_at', array(Carbon::yesterday(), Carbon::now()->addDay()))->count();

        return response()->json(['countTransactions'=>$countTransactions, 'countUsers'=>$countUsers]);
    }
}