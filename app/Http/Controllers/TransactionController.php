<?php

namespace milesfair\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use milesfair\Http\Requests;
use milesfair\Models\Adjudication;
use milesfair\Models\Airline;
use milesfair\Models\Airport;
use milesfair\Models\Customer;
use milesfair\Models\Evidence;
use milesfair\Models\Message;
use milesfair\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use milesfair\Exceptions\PayPalConnectionException;
use milesfair\Services\PayPalService;

class TransactionController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate', 'downloadEvidence']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $transactions = Transaction::with(['buyer', 'seller'])->get();

        return response()->json(['transactions'=>$transactions]);
    }

    /**
     * Display today's new listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNewTransactions()
    {
        //
        $transactions = Transaction::whereBetween('created_at', array(Carbon::yesterday(), Carbon::now()->addDay()))->get(['id', 'noOfTicket', 'status', 'price', 'created_at']);

        return response()->json(['transactions'=>$transactions]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $transaction = Transaction::find($id);
        $buyer = Customer::where('user_id', $transaction->buyer_id)->first(['firstname', 'lastname', 'avatar_url']);
        $seller = Customer::where('user_id', $transaction->seller_id)->first(['firstname', 'lastname', 'avatar_url']);
        $departureAirport = Airport::find($transaction->departureAirport_id);
        $arrivalAirport = Airport::find($transaction->arrivalAirport_id);
        $airline = Airline::find($transaction->airline_id);
        $messages = Message::where('transaction_id', $transaction->id)->get();
        $evidences = Evidence::with('uploader')->where('transaction_id', $transaction->id)->get();
        $adjudication = Adjudication::where('transaction_id', $transaction->id)->first();

        $transaction->buyer = $buyer;
        $transaction->seller = $seller;
        $transaction->departureAirport = $departureAirport;
        $transaction->arrivalAirport = $arrivalAirport;
        $transaction->airline = $airline;
        $transaction->messages = $messages;
        $transaction->evidences = $evidences;
        $transaction->adjudication = $adjudication;

        return response()->json(['transaction'=>$transaction]);
    }

    /**
     * Download evidence
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadEvidence(){

        $txnId = Input::get('id');
        $uploaderId = Input::get('uploaderId');
        $filename = Input::get('filename');
        $txn = Transaction::find($txnId);

        $entry = $txn->evidences()
            ->where('uploader','=',$uploaderId)
            ->where('file_name','=',$filename)
            ->first();

        $file = $entry->getEvidenceFile();

        $response = response($file, 200, [
            'Content-Type' => $entry->file_mine,
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => "attachment; filename={$entry->file_name}",
            'Content-Transfer-Encoding' => 'binary',
        ]);

        ob_end_clean(); // <- this is important, i have forgotten why.

        return $response;

    }

    /**
     * Trigger refund process
     *
     * @param  string note
     * @return \Illuminate\Http\Response::json
     */
    public function refundAdjudication( ){
        $txId = Input::get('txId');
        $note = Input::get('note');

        DB::beginTransaction();

        //update transaction status
        $txn = Transaction::findOrFail($txId);
        $txn->refundPayment();
        $txn->save();

        // call PayPal to refund
        $payKey = $txn->payment->pay_key;
        $response = PayPalService::refundPayment($payKey);
        if(strtoupper($response->responseEnvelope->ack)!=='SUCCESS')
            throw new PayPalConnectionException($response);

        //create adjudication record
        $adj = new Adjudication;
        $adj->transaction_id = $txId;
        $adj->result = Adjudication::refund();
        $adj->reason = $note;
        $adj->adjudicator = Auth::user()->id;
        $adj->adjudicateDT = (new \DateTime())->format(\DateTime::ISO8601);
        $adj->save();

        DB::commit();

        return response()->json(['success'=>true]);
    }

    /**
     * Reject refund process
     *
     * @param  string note
     * @return \Illuminate\Http\Response::json
     */
    public function dismissRefundAdjudication( ){
        $txId = Input::get('txId');
        $note = Input::get('note');

        DB::beginTransaction();

        //update transaction status
        $txn = Transaction::findOrFail($txId);
        $txn->dismissComplaint();
        $txn->save();

        // call PayPal to finish transaction
        $payKey = $txn->payment->pay_key;
        $response = PayPalService::executePayment($payKey);

        if($response->paymentExecStatus=='ERROR'){
            throw new PayPalConnectionException('Error when complete payment');
        }

        if((strtoupper($response->responseEnvelope->ack) !== 'SUCCESS')
            || ($response->paymentExecStatus !=='COMPLETED')){
            throw new \Exception('Error when complete payment');
        }

        //create adjudication record
        $adj = new Adjudication;
        $adj->transaction_id = $txId;
        $adj->result = Adjudication::dismiss();
        $adj->reason = $note;
        $adj->adjudicator = Auth::user()->id;
        $adj->adjudicateDT = (new \DateTime())->format(\DateTime::ISO8601);
        $adj->save();

        DB::commit();

        return response()->json(['success'=>true]);
    }
}
