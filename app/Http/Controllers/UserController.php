<?php

namespace milesfair\Http\Controllers;

use Illuminate\Http\Request;
use milesfair\Http\Requests;
use milesfair\Models\Customer;
use milesfair\Models\User;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::where('is_admin', '!=', true)->get(['id','email','is_blocked','is_activate','created_at']);

        return response()->json(['users'=>$users]);

    }

    /**
     * get users by today.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTodayNewUsers()
    {
        //
        $users = User::whereBetween('created_at', array(Carbon::yesterday(), Carbon::now()->addDay()))->get(['id','email','is_blocked','is_activate','created_at']);

        return response()->json(['users'=>$users]);

    }

    /**
     * Ban use.
     *
     * @return JSON
     */
    public function ban($id)
    {
        //
        $user = User::find($id);
        $user->is_blocked = 1;
        return response()->json(['result'=>$user->save()]);
    }

    /**
     * ubBan use.
     *
     * @return JSON
     */
    public function unban($id)
    {
        //
        $user = User::find($id);
        $user->is_blocked = 0;
        return response()->json(['result'=>$user->save()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $customer = Customer::where('user_id', $id)->first(['id', 'user_id', 'firstname', 'lastname', 'avatar_url', 'birthday', 'country', 'languages', 'addr_city', 'country', 'phoneNo']);

        return response()->json(['customer'=>$customer]);
    }
}
