<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get("/", "HomeController@index")->name('login');

Route::get('auth/login', function(){
    return redirect()->route('login');
});
Route::post('auth/login', 'Auth\\AuthController@postLogin');
Route::get('auth/logout', 'Auth\\AuthController@getLogout');

// Password reset link request routes...
Route::get('auth/forget', 'Auth\\PasswordController@getEmail');
Route::post('auth/forget', 'Auth\\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// change password
Route::get('password/change', 'Auth\PasswordController@loadChangePasswordView');
Route::post('password/change', 'Auth\PasswordController@changePassword');

Route::get('/dashboard', ['middleware' => 'auth', 'uses' => 'HomeController@dashboard']);

//download evidence
Route::post('download_evidence', 'TransactionController@downloadEvidence');

/* User RESTful API routes */
Route::group(['prefix' => 'api'], function()
{
    Route::resource('users', 'UserController');
    Route::get('users/ban/{id}', 'UserController@ban');
    Route::get('users/unban/{id}', 'UserController@unban');
    Route::get('newusers', 'UserController@getTodayNewUsers');

    Route::get('transactions', 'TransactionController@index');
    Route::get('transactions/{id}', 'TransactionController@show');
    Route::get('newtransactions', 'TransactionController@getNewTransactions');
    Route::post('transactions/agree_dispute', 'TransactionController@refundAdjudication');
    Route::post('transactions/dismiss_dispute', 'TransactionController@dismissRefundAdjudication');

    Route::get('countbytoday', 'DashboardController@countByToday');
});