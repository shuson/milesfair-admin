<?php

namespace milesfair\Models;

use Illuminate\Database\Eloquent\Model;

class Adjudication extends Model
{
    //
    public static function refund(){
        return 'Refunded';
    }

    public static function dismiss(){
        return 'Dismissed';
    }
}
