<?php

namespace milesfair\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Airline extends Model
{
    //

    public function supportingAirlines(){
        return $this->hasMany(SupportingAirline::class);
    }


    /**
     * Update airlines.lowestPrice by traverse all customer's supporting_airlines
     *   excluding following cases
     *     - user account is locked by Admin
     *     - customer doesn't provide profile info
     */
    public static function updateLowestPrice(){
        $updateStatement =
            " UPDATE airlines as airline_table ".
            " INNER JOIN ( ".
            "     SELECT t1.id, ".
            "        Coalesce(min(t2.pricePerMile), 0) as lowest_price ".
            "     FROM airlines as t1 ".
            "     LEFT JOIN ( ".
            "         SELECT supporting_airlines.* ".
            "         FROM customers, supporting_airlines, users ".
            "         WHERE customers.id = supporting_airlines.customer_id ".
            "         AND users.id = customers.user_id ".
            "         AND users.is_blocked = '0' ".
            "         AND customers.is_completed = '1' ".
            "      ) as t2 ".
            "      ON t1.id = t2.airline_id ".
            "      GROUP BY t1.id ".
            " ) AS price_table ".
            " ON airline_table.id = price_table.id ".
            " SET airline_table.lowestPrice= price_table.lowest_price "
        ;

        $affected = DB::update($updateStatement);
//        echo $affected;
    }

    /**
     * @param $airlineName
     */
    public static function findByName($airlineName){
        $airline = DB::table('airlines')->where('airlineCode', $airlineName)->first();
        return $airline;
    }

}
