<?php

namespace milesfair\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Airport extends Model
{
    /**
     * Retrieve Mileage from DB based on departure, arrival airport
     *  and cabin class
     * @param $route
     * @param $cabin
     * @param $type one-way:1 route:2
     * @return int
     */
    public static function getMileage($route,$cabin,$type){
        $departureCode = $route->outbound_segments[0]->departure_code;
        $arriveCode = $route->outbound_segments[count($route->outbound_segments) - 1]->arrival_code;

        //get mileage from DB
        $query = DB::table('mileage_spent_mappings')
            ->join('airports as depart', 'mileage_spent_mappings.originArea','=', 'depart.area')
            ->join('airports as arri', 'mileage_spent_mappings.destinationArea','=', 'arri.area')
            ->where('depart.airportCd','=',$departureCode)
            ->where('arri.airportCd','=',$arriveCode)
            ->where('mileage_spent_mappings.cabinClass','=',$cabin)
            ->select('mileage_spent_mappings.mileage', 'depart.area as departArea', 'arri.area as ArriArea');

        $mileage = $query->first();
        //dump($mileage);
        return $mileage->mileage * $type;
    }

    /**
     * @param $airlineName
     */
    public static function findByAirportCode($airportCd){
        $airport = DB::table('airports')->where('airportCd', $airportCd)->first();
        return $airport;
    }
}
