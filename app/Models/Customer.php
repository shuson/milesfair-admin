<?php

namespace milesfair\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Customer extends Model
{

    protected $table = "customers";
    /**
     * Define pre filled
     * @var array
     */
    protected $fillable = array('firstname', 'lastname', 'birthday', 'avatar_url', 'country', 'addr_city', 'addr_block', 'languages', 'self_description', 'phoneNo');

    public function supportingAirlines()
    {
        return $this->hasMany('milesfair\Models\SupportingAirline');
    }

    /**
     * Customers
     *
     * @var array
     */
    protected $attributes = [
        'is_completed' => false,
    ];

    /**
     * Query the user that belongs to the customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function searchSeller($searchCriteria){

        $query = DB::table('customers')
            ->join('supporting_airlines', 'customers.id','=', 'supporting_airlines.customer_id')
            ->join('airlines','supporting_airlines.airline_id','=','airlines.id')
            ->where('airlines.airlineCode','=' , $searchCriteria->airlineCd)
            ->where('supporting_airlines.availableMileage','>=',$searchCriteria->noOfTicket * $searchCriteria->mileage)
            ->select('customers.*', 'supporting_airlines.pricePerMile')
            ->orderBy('supporting_airlines.pricePerMile', 'asc');

        return $query->get();
    }

    public static function updateSellerReviewRating($customerId){
        $updateStatement =
            ' UPDATE customers '.
            ' SET rating_as_seller = ( '.
            '   SELECT round(avg(reviews.rating )) '.
            '   FROM `transactions`, `reviews` '.
            '   WHERE transactions.seller_id = reviews.reviewee_id '.
            '   AND transactions.id = reviews.transaction_id '.
            '   AND transactions.seller_id =  ?'.
            ' )'.
            ' WHERE ' . 'customers.id =  ?'
        ;

        $affected = DB::update($updateStatement,[$customerId,$customerId]);
//            ->setBindings([$customerId,$customerId]);
//        echo $affected;
    }

    public static function updateBuyerReviewRating($customerId){
        $updateStatement =
            ' UPDATE customers '.
            ' SET rating_as_buyer = ( '.
            '   SELECT round(avg(reviews.rating )) '.
            '   FROM `transactions`, `reviews` '.
            '   WHERE transactions.buyer_id = reviews.reviewee_id '.
            '   AND transactions.id = reviews.transaction_id '.
            '   AND transactions.buyer_id = ? '.
            ' )'.
            ' WHERE ' . 'customers.id = ? '
        ;

        $affected = DB::update($updateStatement,[$customerId,$customerId]);
//        echo $affected;
    }

}
