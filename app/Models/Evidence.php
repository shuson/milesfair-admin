<?php

namespace milesfair\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Evidence extends Model
{

    public function uploader()
    {
        return $this->hasOne('milesfair\Models\Customer', 'id', 'uploader');
    }

    public static function saveEvidence($txnId,$uploaderId,$file,$statement,$postDT){

//        $extension       = $file->getClientOriginalExtension();

//        $destinationPath = 'storage/evidences/' . $uploaderId .'/'.date("Ym", time()) .'/'.date("d", time()) ;
//        $safeName        = str_random(10).'.'.$extension;
//        $file->move($destinationPath, $safeName);

        Storage::disk(config('filesystem.default'))->put('Evidence'.'/'.$txnId.'/'.$uploaderId.'/'.$file->getClientOriginalName(),
            File::get($file));
        $evidence = new Evidence();
        $evidence->transaction_id = $txnId;
        $evidence->uploader = $uploaderId;
        $evidence->statement = $statement;
        $evidence->postDT = $postDT;
        $evidence->file_name = $file->getClientOriginalName();
        $evidence->file_mime = $file->getClientMimeType();
        $evidence->save();

        return $evidence;
    }

    /**
     * @return mixed
     */
    public function getEvidenceFile(){
        return Storage::disk(config('filesystem.default'))
            ->get('Evidence'.'/'.$this->transaction_id.'/'.$this->uploader.'/'.$this->file_name);
    }
}
