<?php

namespace milesfair\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Message extends Model
{

    const C_Make_Offer = '%s made an offer.';
    const C_Cancel = '%s cancelled an transaction.';
    const C_Reject_Offer = '%s declined an offer.';
    const C_Accept_Offer = '%s accepted an offer.';
    const C_Enter_Passenger = '%s provided passenger information.';
    const C_Finish_Payment = '%s made the payment.';
    const C_Provide_Ticket = '%s provided the ticket.';
    const C_Refund = 'Payment refunded.';
    const C_Complete = 'Transaction completed.';
    const C_Submit_Dispute = '%s submitted a dispute.';

//region setter/getter
    public function sender()
    {
        return $this->hasOne('milesfair\Models\Customer', 'id', 'sender_id');
    }

    public function receiver()
    {
        return $this->hasOne('milesfair\Models\Customer', 'id', 'receiver_id');
    }

    public function transaction()
    {
        return $this->belongsTo('milesfair\Models\Transaction', 'id', 'transaction_id');
    }
//endregion

//region public static methods

    public static function newMessage($transactionId,$senderId,$receiverId,$body,$current_dt){
        $msg = new Message();
        $msg->transaction_id = $transactionId;
        $msg->sender_id = $senderId;
        $msg->receiver_id = $receiverId;
        $msg->body = $body;
        $msg->post_datetime = $current_dt;
        $msg->save();

        return $msg;
    }

    /**
     * Retrieve messages for a particular transaction and update their [is_read] => true
     *
     * @param $txnId
     * @param $userId
     * @return mixed
     */
    public static function getMessagesByTimestamp($timestamp,$customerId){

        // where statement delegate
        $statement = function($query) use ($timestamp,$customerId){
            $query->where('post_datetime','>=',$timestamp)
                ->where(function ($userQuery) use($customerId) {
                    $userQuery->where('sender_id','=',$customerId)
                        ->orWhere('receiver_id','=',$customerId);
                });
        };

        // retrieve messages
        $messages = Message::where($statement)
            ->orderBy('transaction_id', 'asc')
            ->orderBy('post_datetime', 'asc')
            ->get(['transaction_id','id','sender_id','receiver_id','body','post_datetime','is_read']);

        return $messages;
    }

    /**
     * Update message status that has been read.
     * For those msg that $receiverId currently reading($txnId) and which is posted before $timestamp
     *
     * @param $timestamp
     * @param $txnId
     * @param $receiverId
     */
    public static function setMessageRead($timestamp,$txnId,$receiverId){

        // update to be read
        $affectedRow = DB::table('messages')
            ->where('transaction_id','=',$txnId)
            ->where('receiver_id','=',$receiverId)
            ->where('post_datetime','<',$timestamp)
            ->where('is_read','=',false)
            ->update(['is_read' => true]);

        return $affectedRow;
    }


//endregion
}
