<?php

namespace milesfair\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Notification extends Model
{

    const C_New_Message = 'You have a new message.';
    const C_Passenger_Details = 'Input Passenger Details.';
    const C_Process_Order = 'Payment complete. Please process order.';
    const C_Ticket_Confirmation = 'Ticket Confirmation.';
    const C_Leave_Review = 'Please leave a review.';
    const C_Pending_Dispute = 'Pending Dispute.';

//region setter/getter
    public function receiver()
    {
        return $this->hasOne('milesfair\Models\Customer', 'id', 'receiver_id');
    }

    public function transaction()
    {
        return $this->hasOne('milesfair\Models\Transaction', 'id', 'transaction_id');
    }
//endregion

//region public static methods

    public static function newNotification($receiverId,$type,$body,$current_dt){
        $notice = new Notification();
        $notice->receiver_id = $receiverId;
        $notice->type = $type;
        $notice->body = $body;
        $notice->post_datetime = $current_dt;
        $notice->save();
    }

    /**
     * @param $customerId
     * @return mixed
     */
    public static function getNotifications($customerId){
        $result = Notification::where('receiver_id','=',$customerId)
            ->orderBy('post_datetime', 'asc')
            ->get(['id','type','body','post_datetime']);

        return $result;
    }

    /**
     * @param $id
     * @param $customerId
     * @return mixed
     */
    public static function deleteNotifications($id,$customerId){
        $deletedRows = Notification::where('id','=',$id)
            ->where('receiver_id','=',$customerId)
            ->delete();

        return $deletedRows;
    }

    /**
     * @param $customerId
     * @return mixed
     */
    public static function clearNotifications($customerId){
        $deletedRows = Notification::where('receiver_id','=',$customerId)
            ->delete();

        return $deletedRows;
    }

//endregion
}
