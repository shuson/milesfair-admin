<?php

namespace milesfair\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}
