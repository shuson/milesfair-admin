<?php

namespace milesfair\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public function reviewer()
    {
        return $this->hasOne('milesfair\Models\Customer', 'id', 'reviewer_id');
    }

    public function reviewee()
    {
        return $this->hasOne('milesfair\Models\Customer', 'id', 'reviewee_id');
    }

    public function transaction()
    {
        return $this->hasOne('milesfair\Models\Transaction', 'id', 'transaction_id');
    }


    public static function newReview($txn,$senderId,$receiverId,$comment,$rating,$postDT){
        $review = new Review();
        $review->transaction_id = $txn->id;
        $review->reviewer_id = $senderId;
        $review->reviewee_id = $receiverId;
        $review->comment = $comment;
        $review->rating = $rating;
        $review->post_datetime = $postDT;
        $review->save();

        if($txn->buyer_id===$receiverId) {
            Customer::updateBuyerReviewRating($receiverId);
        }else {
            Customer::updateSellerReviewRating($receiverId);
        }

        return $review;
    }
}
