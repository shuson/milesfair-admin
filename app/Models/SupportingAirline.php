<?php

namespace milesfair\Models;

use Illuminate\Database\Eloquent\Model;

class SupportingAirline extends Model
{
    protected $table = 'supporting_airlines';

    public function airline()
    {
        return $this->belongsTo(Airline::class, 'airline_id');
    }
}
