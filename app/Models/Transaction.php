<?php

namespace milesfair\Models;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use milesfair\Models\TransactionState\InitiatedState;
use milesfair\Models\TransactionState\TransactionState;

class Transaction extends Model
{

    private $__state;

    public function __construct($attributes = array(), TransactionState $state = null)
    {
        parent::__construct($attributes);
        $this->__state = $state;
    }

    public function state()
    {
        if (null == $this->__state) {

            $state_class = __NAMESPACE__ . "\\TransactionState\\" . $this->status . "State";
            $this->setState(new $state_class($this));
        }
        return $this->__state;
    }

    public function setState(TransactionState $state)
    {
        $this->__state = $state;
        // status represent the value of state stored in DB
        $this->status = $state->stateName();
    }


//region objects getters

    public function buyer()
    {
        return $this->hasOne('milesfair\Models\Customer', 'id', 'buyer_id');
    }

    public function seller()
    {
        return $this->hasOne('milesfair\Models\Customer', 'id', 'seller_id');
    }

    public function departure()
    {
        return $this->hasOne('milesfair\Models\Airport', 'id', 'departureAirport_id');
    }

    public function arrive()
    {
        return $this->hasOne('milesfair\Models\Airport', 'id', 'arriveAirport_id');
    }

    public function airline()
    {
        return $this->hasOne('milesfair\Models\Airline', 'id', 'airline_id');
    }

//    public function segments()
//    {
//        return $this->hasMany('milesfair\Models\Segment');
//    }

    public function messages()
    {
        return $this->hasMany('milesfair\Models\Message');
    }

    public function reviews()
    {
        return $this->hasMany('milesfair\Models\Review');
    }

    public function payment()
    {
        return $this->hasOne('milesfair\Models\Payment');
    }

//    public function passengers()
//    {
//        return $this->hasMany('milesfair\Models\Passenger');
//    }
//
//    public function contact()
//    {
//        return $this->hasOne('milesfair\Models\Contact');
//    }

    public function adjudication()
    {
        return $this->hasOne('milesfair\Models\Adjudication');
    }

    public function evidences()
    {
        return $this->hasMany('milesfair\Models\Evidence');
    }

//endregion

//region public methods
    public function initiate(){
        if((null===$this->id)&&('' == $this->status)){
            $this->status = InitiatedState::stateName();
            $this->state();
        }
        else{
            echo "illegal operation, cannot initiate a existing txn".PHP_EOL;
        }
    }

    public function makeOffer($customerName){
        $this->state()->makeOffer();
        $this->generateSystemMessage(Message::C_Make_Offer,$customerName);
    }

    public function cancel($customerName){
        $this->state()->cancel();
        $this->generateSystemMessage(Message::C_Cancel,$customerName);
    }

    public function rejectOffer($customerName){
        $this->state()->rejectOffer();
        $this->generateSystemMessage(Message::C_Reject_Offer,$customerName);
    }

    public function acceptOffer($customerName){
        $this->state()->acceptOffer();
        $this->generateSystemMessage(Message::C_Accept_Offer,$customerName);
    }

    public function enterPassenger($customerName){
        $this->state()->enterPassenger();
        $this->generateSystemMessage(Message::C_Enter_Passenger,$customerName);
    }

    public function makePayment(){
        $this->state()->makePayment();
    }

    public function finishPayment($customerName){
        $this->state()->finishPayment();
        $this->generateSystemMessage(Message::C_Finish_Payment,$customerName);
    }

    public function provideTicket($customerName){
        $this->state()->provideTicket();
        $this->generateSystemMessage(Message::C_Provide_Ticket,$customerName);
    }

    public function depart(){
        $this->state()->depart();
    }

    public function refundPayment(){
        $this->state()->refundPayment();
        $this->generateSystemMessage(Message::C_Refund);
    }

    public function complete(){
        $this->state()->complete();
        $this->generateSystemMessage(Message::C_Complete);
    }

    public function submitComplaint($customerName){
        $this->state()->submitComplaint();
        $this->generateSystemMessage(Message::C_Complete,$customerName);
    }

    public function dismissComplaint(){
        $this->state()->dismissComplaint();
    }

    public function attachReview($review){
        $this->state()->attachReview($review);
    }

    public function attachMessage($msg){
        $this->state()->attachMessage($msg);
    }
//endregion

//region public static methods

    public static function getTransactionsByTimestamp($timestamp=0,$customerId){

        // get buyer/seller 's name and avatar
        $customerQuery = function($query)
        {
            $query->select('id','firstname', 'lastname', 'avatar_url');
        };

        $transactions = Transaction::
            where(function ($userQuery) use($customerId) {
                $userQuery->where('buyer_id','=',$customerId)
                    ->orWhere('seller_id','=',$customerId);
            })
            ->where('updated_at','>=',$timestamp)
            ->with(array('buyer' => $customerQuery))
            ->with(array('seller' => $customerQuery))
            ->with('reviews')
            ->with('evidences')
            ->with('adjudication')
        ->get();

        return $transactions;
    }

    /**
     * retrieve particular transaction for either buyer or seller
     * @param $id
     * @param $customerId
     * @return mixed
     */
    public static function getTransactionForCustomer($id,$customerId){
        $customerQuery = function($query) use($customerId)
        {
            $query->where('seller_id','=',$customerId)
            ->orWhere('buyer_id','=',$customerId);
        };

        $transaction = Transaction::where('id',$id)
            ->where($customerQuery)
            ->firstOrFail();
        return $transaction;
    }

    /**
     * retrieve particular transaction for buyer
     * @param $id
     * @param $buyerId
     * @return mixed
     */
    public static function getTransactionForBuyer($id,$buyerId){
        $transaction = Transaction::where('id','=',$id)
            ->where('buyer_id','=',$buyerId)
            ->firstOrFail();
        return $transaction;
    }

    /**
     * retrieve particular transaction for seller
     * @param $id
     * @param $sellerId
     * @return mixed
     */
    public static function getTransactionForSeller($id,$sellerId){
        $transaction = Transaction::where('id','=',$id)
            ->where('seller_id','=',$sellerId)
            ->firstOrFail();
        return $transaction;
    }

//endregion

    private function generateSystemMessage($format,$customerName = null){

        $current_dt = (new DateTime())->format(DateTime::ISO8601);

        //
        $msgToBuyer = Message::newMessage($this->id,
            null,
            $this->buyer_id,
            sprintf($format,$customerName),
            $current_dt);

        $this->messages()->save($msgToBuyer);

        //
        $msgToSeller = Message::newMessage($this->id,
            null,
            $this->seller_id,
            sprintf($format,$customerName),
            $current_dt);

        $this->messages()->save($msgToSeller);

    }
}
