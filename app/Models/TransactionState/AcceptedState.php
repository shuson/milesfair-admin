<?php
/**
 * Created by PhpStorm.
 * User: XUMS
 * Date: 2015/10/10
 * Time: 1:03
 */

namespace milesfair\Models\TransactionState;


class AcceptedState extends TransactionState{
    public static function stateName(){
        return 'Accepted';
    }

    public function cancel(){
        parent::_cancelTransaction();
    }

    public function enterPassenger(){
//        echo $this->stateName().' - overrode enterPassenger'.PHP_EOL;
        $this->_transaction->setState(new WaitForPaymentState($this->_transaction));
    }

    public function attachMessage($msg){
        parent::_attachMessage($msg);
    }

}