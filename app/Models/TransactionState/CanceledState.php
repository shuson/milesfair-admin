<?php
/**
 * Created by PhpStorm.
 * User: XUMS
 * Date: 2015/10/10
 * Time: 1:03
 */

namespace milesfair\Models\TransactionState;


class CanceledState extends TransactionState{
    public static function stateName(){
        return 'Canceled';
    }

}