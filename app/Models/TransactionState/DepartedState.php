<?php
/**
 * Created by PhpStorm.
 * User: XUMS
 * Date: 2015/10/10
 * Time: 1:03
 */

namespace milesfair\Models\TransactionState;


class DepartedState extends TransactionState{
    public static function stateName(){
        return 'Departed';
    }

    public function complete(){
//        echo $this->stateName().' - overrode complete'.PHP_EOL;
        $this->_transaction->setState(new CompletedState($this->_transaction));
    }

    public function attachReview($review){
        $this->_transaction->reviews()->save($review);
    }

//    public function submitComplaint(){
//        echo $this->stateName().' - overrode submitComplaint'.PHP_EOL;
//        $this->_transaction->setState(new DisputedState($this->_transaction));
//    }
//
    public function attachMessage($msg){
        parent::_attachMessage($msg);
    }

}