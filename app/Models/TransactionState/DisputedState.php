<?php
/**
 * Created by PhpStorm.
 * User: XUMS
 * Date: 2015/10/10
 * Time: 1:03
 */

namespace milesfair\Models\TransactionState;


class DisputedState extends TransactionState{
    public static function stateName(){
        return 'Disputed';
    }

    public function dismissComplaint(){
//        echo $this->stateName().' - overrode refundPayment'.PHP_EOL;
        $this->_transaction->setState(new TicketProvidedState($this->_transaction));
    }

    public function refundPayment(){
//        echo $this->stateName().' - overrode refundPayment'.PHP_EOL;
        $this->_transaction->setState(new RefundedState($this->_transaction));
    }


    public function attachMessage($msg){
        parent::_attachMessage($msg);
    }

}