<?php
/**
 * Created by PhpStorm.
 * User: XUMS
 * Date: 2015/10/10
 * Time: 1:03
 */

namespace milesfair\Models\TransactionState;


class InitiatedState extends TransactionState{
    public static function stateName(){
        return 'Initiated';
    }

    public function makeOffer(){
//        echo $this->stateName().' - overrode makeOffer'.PHP_EOL;
        $this->_transaction->setState(new OfferMadeState($this->_transaction));
    }

    public function cancel(){
        parent::_cancelTransaction();
    }

    public function attachMessage($msg){
        parent::_attachMessage($msg);
    }

}