<?php
/**
 * Created by PhpStorm.
 * User: XUMS
 * Date: 2015/10/10
 * Time: 1:03
 */

namespace milesfair\Models\TransactionState;


class OfferMadeState extends TransactionState{
    public static function stateName(){
        return 'OfferMade';
    }

    public function cancel(){
        parent::_cancelTransaction();
    }

    /**
     *
     */
    public function rejectOffer(){
//        echo $this->stateName().' - overrode rejectOffer'.PHP_EOL;
        $this->_transaction->setState(new RejectedState($this->_transaction));
    }

    public function acceptOffer(){
//        echo $this->stateName().' - overrode acceptOffer'.PHP_EOL;
        $this->_transaction->setState(new AcceptedState($this->_transaction));
    }

    public function attachMessage($msg){
        parent::_attachMessage($msg);
    }

}