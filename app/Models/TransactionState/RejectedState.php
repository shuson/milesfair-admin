<?php
/**
 * Created by PhpStorm.
 * User: XUMS
 * Date: 2015/10/10
 * Time: 1:03
 */

namespace milesfair\Models\TransactionState;


class RejectedState extends TransactionState{
    public static function stateName(){
        return 'Rejected';
    }

    public function cancel(){
        parent::_cancelTransaction();
    }

    public function makeOffer(){
//        echo $this->stateName().' - overrode makeOffer'.PHP_EOL;
        $this->_transaction->setState(new OfferMadeState($this->_transaction));
    }

    public function attachMessage($msg){
        parent::_attachMessage($msg);
    }

}