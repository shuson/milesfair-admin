<?php
/**
 * Created by PhpStorm.
 * User: XUMS
 * Date: 2015/10/10
 * Time: 1:03
 */

namespace milesfair\Models\TransactionState;


class TicketProvidedState extends TransactionState{
    public static function stateName(){
        return 'TicketProvided';
    }

    public function depart(){
//        echo $this->stateName().' - overrode complete'.PHP_EOL;
        $this->_transaction->setState(new DepartedState($this->_transaction));
    }

    public function submitComplaint(){
//        echo $this->stateName().' - overrode submitComplaint'.PHP_EOL;
        $this->_transaction->setState(new DisputedState($this->_transaction));
    }

    public function attachMessage($msg){
        parent::_attachMessage($msg);
    }

}