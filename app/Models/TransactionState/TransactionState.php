<?php
/**
 * Created by PhpStorm.
 * User: XUMS
 * Date: 2015/10/10
 * Time: 0:46
 */

namespace milesfair\Models\TransactionState;


use milesfair\Exceptions\IllegalOperationException;

abstract class TransactionState {

    protected $_transaction;

    public function __construct($transaction)
    {
        $this->_transaction = $transaction;
    }

    public static function stateName(){
        return 'AbstractClass';
    }

//region state methods

    public function cancel(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function makeOffer(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function rejectOffer(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function acceptOffer(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function enterPassenger(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function makePayment(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function finishPayment(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function provideTicket(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function depart(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function refundPayment(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function complete(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function submitComplaint(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function dismissComplaint(){
        throw new IllegalOperationException('Illegal operation');
    }

    public function attachReview($review){
        throw new IllegalOperationException('Illegal operation');
    }

    public function attachMessage($msg){
        throw new IllegalOperationException('Illegal operation');
    }
//endregion

//region shared protected methods
    protected function _cancelTransaction(){
        echo $this->stateName().' - overrode cancel'.PHP_EOL;
        $this->_transaction->setState(new CanceledState($this->_transaction));
    }

    protected function _attachMessage($msg){
        $this->_transaction->messages()->save($msg);
    }
//endregion


}