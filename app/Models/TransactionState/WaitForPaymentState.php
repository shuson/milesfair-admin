<?php
/**
 * Created by PhpStorm.
 * User: XUMS
 * Date: 2015/10/10
 * Time: 1:03
 */

namespace milesfair\Models\TransactionState;

class WaitForPaymentState extends TransactionState{
    public static function stateName(){
        return 'WaitForPayment';
    }

    public function cancel(){
        parent::_cancelTransaction();
    }

    /**
     *  this method represent buyer click pay and proceed to paypal, which will not change payment state
     */
    public function makePayment(){
//        echo $this->stateName().' - overrode makePayment'.PHP_EOL;

    }

    /**
     *  this method represent that PayPal return a success of payment
     */
    public function finishPayment(){
//        echo $this->stateName().' - overrode finishPayment'.PHP_EOL;
        $this->_transaction->setState(new WaitForTicketState($this->_transaction));
    }

    public function attachMessage($msg){
        parent::_attachMessage($msg);
    }

}