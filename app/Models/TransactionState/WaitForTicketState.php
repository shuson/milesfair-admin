<?php
/**
 * Created by PhpStorm.
 * User: XUMS
 * Date: 2015/10/10
 * Time: 1:03
 */

namespace milesfair\Models\TransactionState;


class WaitForTicketState extends TransactionState{
    public static function stateName(){
        return 'WaitForTicket';
    }

    public function provideTicket(){
//        echo $this->stateName().' - overrode provideTicket'.PHP_EOL;
        $this->_transaction->setState(new TicketProvidedState($this->_transaction));
    }

    public function refundPayment(){
//        echo $this->stateName().' - overrode refundPayment'.PHP_EOL;
        $this->_transaction->setState(new RefundedState($this->_transaction));
    }

    public function attachMessage($msg){
        parent::_attachMessage($msg);
    }

}