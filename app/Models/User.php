<?php

namespace milesfair\Models;

use milesfair\Presenters\UserPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use McCool\LaravelAutoPresenter\HasPresenter;


class User extends Model implements AuthenticatableContract, CanResetPasswordContract, hasPresenter
{
    use Authenticatable, CanResetPassword, SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * Define pre filled
     * @var array
     */

    protected $fillable = array('email', 'password');

    /**
     * Users should not be admin by default.
     *
     * @var array
     */
    protected $attributes = [
        'is_admin' => false,
        'is_blocked' => false,
        'is_activate' => false,
        'deleted_at' => null,
        'confirmation_code' => null,
    ];

    /**
     * Query the user's customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customer()
    {
        return $this->hasOne(Customer::class);
    }

    /**
     * Check user's permissions.
     *
     * @return bool
     */
    public function isAdmin()
    {
        return ($this->is_admin == true);
    }

    /**
     * Check user's accessibility.
     *
     * @return bool
     */
    public function isBlocked()
    {
        return ($this->is_blocked == true);
    }

    /**
 * Check user's activation status.
 *
 * @return bool
 */
    public function isActivated()
    {
        return ($this->is_activate == true);
    }

    /**
     * confirmation code for email verification
     *
     * @return string(30)
     */
    public function getConfirmationCode()
    {
        return ($this->confirmation_code);
    }

    public function getPresenterClass()
    {
        return UserPresenter::class;
    }
}
