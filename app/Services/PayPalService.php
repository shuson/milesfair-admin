<?php
/**
 * Created by PhpStorm.
 * User: XUMS
 * Date: 2015/8/20
 * Time: 14:39
 */

namespace milesfair\Services;


use PayPal\IPN\PPIPNMessage;
use PayPal\Service\AdaptivePaymentsService;
use PayPal\Types\AP\ExecutePaymentRequest;
use PayPal\Types\AP\PaymentDetailsRequest;
use PayPal\Types\AP\PayRequest;
use PayPal\Types\AP\Receiver;
use PayPal\Types\AP\ReceiverList;
use PayPal\Types\AP\RefundRequest;
use PayPal\Types\Common\RequestEnvelope;
use Psy\Exception\Exception;
use stdClass;

class PayPalService {

    private $_api_context;

    private function __construct()
    {
    }

    /**
     * @param $payment
     * @return \PayPal\Service\Types\AP\PayResponse
     * @throws Exception
     * @throws \Exception
     */
    public static function postPayment($payment){

        $receiver = array();
        // facilitator
        $receiver[0] = new Receiver();
        $receiver[0]->email = $payment->facilitator;
        $receiver[0]->amount = $payment->itemQuantity * $payment->itemPrice;
        $receiver[0]->primary = TRUE;
        $receiver[0]->paymentType = 'SERVICE';

        //seller
        $receiver[1] = new Receiver();
        $receiver[1]->email = $payment->seller;
        $receiver[1]->amount = $payment->itemQuantity * $payment->itemPrice * 0.95; // 5% service charge
        $receiver[1]->primary = FALSE;
        $receiver[1]->paymentType = 'SERVICE';

        $receiverList = new ReceiverList($receiver);

        $actionType = 'PAY_PRIMARY';
        $currencyCd = 'USD';

        $payRequest = new PayRequest(
            new RequestEnvelope("en_US"),
            $actionType,
            $payment->returnUrl,
            $currencyCd,
            $receiverList,
            $payment->cancelUrl);

        $payRequest->feesPayer = 'SECONDARYONLY';
        $payRequest->senderEmail = $payment->buyer;

        $service = new AdaptivePaymentsService(config('paypal.api_context'));
        try {
            /* wrap API method calls on the service object with a try catch */
            $response = $service->Pay($payRequest);
        } catch (Exception $ex) {
            throw $ex;
        }

        return $response;
    }

    /**
     * @param $payKey
     * @return \PayPal\Service\Types\AP\PaymentDetailsResponse
     * @throws Exception
     * @throws \Exception
     */
    public static function getPaymentStatus($payKey){
        $requestEnvelope = new RequestEnvelope("en_US");
        /*
         * 		 PaymentDetailsRequest which takes,
                 `Request Envelope` - Information common to each API operation, such
                 as the language in which an error message is returned.
         */
        $paymentDetailsReq = new PaymentDetailsRequest($requestEnvelope);

        $paymentDetailsReq->payKey = $payKey;

        $service = new AdaptivePaymentsService(config('paypal.api_context'));
        try {
            /* wrap API method calls on the service object with a try catch */
            $response = $service->PaymentDetails($paymentDetailsReq);
        } catch(Exception $ex) {
            throw $ex;
        }

        return $response;
    }

    /**
     * @param $payKey
     * @return \PayPal\Service\Types\AP\ExecutePaymentResponse
     * @throws Exception
     * @throws \Exception
     */
    public static function executePayment($payKey){
        $executePaymentRequest = new ExecutePaymentRequest(
            new RequestEnvelope("en_US"),
            $payKey);

        $service = new AdaptivePaymentsService(config('paypal.api_context'));
        try {
            /* wrap API method calls on the service object with a try catch */
            $response = $service->ExecutePayment($executePaymentRequest);
        } catch(Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    /**
     * @param $payKey
     * @return \PayPal\Service\Types\AP\RefundResponse
     * @throws Exception
     * @throws \Exception
     */
    public static function refundPayment($payKey){
        $refundRequest = new RefundRequest(new RequestEnvelope("en_US"));

        $refundRequest->payKey = $payKey;
        $refundRequest->currencyCode = 'USD';

        $service = new AdaptivePaymentsService(config('paypal.api_context'));
        try {
            /* wrap API method calls on the service object with a try catch */
            $response = $service->Refund($refundRequest);
        } catch(Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    /**
     * @param $postData
     * @return bool
     */
    public static function processIPN($postData){

        // read data from request
        $ipnMessage = new PPIPNMessage($postData, config('paypal.api_context'));
        foreach($ipnMessage->getRawData() as $key => $value) {
            error_log("IPN: $key => $value");
        }

        $result = new stdClass();
        $result->ipnData = $ipnMessage->getRawData();
        //send request to Paypal for validation
        if($ipnMessage->validate()) {
            error_log("Success: Got valid IPN data");
            $result->isIpnVerified = true;
        } else {
            error_log("Error: Got invalid IPN data");
            $result->isIpnVerified = false;
        }
        return $result;
    }

}