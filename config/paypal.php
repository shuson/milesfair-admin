<?php
/**
 * Created by PhpStorm.
 * User: shuson
 * Date: 29/7/15
 * Time: 11:28 PM
 */

return array(
    // set your paypal credential
    'client_id' => 'AUReDY-hdgOaowHMg37SjA6Oc2di72a-h44V6MuInKyP57EwncYir_GA01_YztRTpf1zsuR_NuyXn8IV',
    'secret' => 'EHxZTsyZMwZhOhR8yqd-th8GCC4wcNZYfDipvLD40wK6ixQQPxNaZ7W8sLc6Dgj5PZGEu6AAs1iL6d6Z',

    'api_context' => array(
        'mode' => 'sandbox',
        'acct1.UserName' => 'milesfair2_api1.163.com',
        'acct1.Password' => 'GEJY4Q9JG2PU7PT9',
        'acct1.Signature' => 'A68nLtIir2p7QZvu1fXYQacxTUL4A.wmnPPgk1RTgNoHecp1Wm1XGJX9',
        "acct1.AppId" => "APP-80W284485P519543T",
    ),

    'transaction_facilitator' => 'milesfair2@163.com',
    'make_payment_base_url'=>'https://www.sandbox.paypal.com/webscr&cmd=_ap-payment&paykey=',

    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);