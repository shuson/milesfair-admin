
@extends('layouts.master')

@section('title', 'Change Password')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Change your password</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @elseif (session('success'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                Your password is changed successfully.
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="/password/change">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group" >
                                <label class="col-md-4 control-label">Old password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="old_password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">New password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="new_password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Confirm password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-5">
                                    <button type="submit" class="btn btn-primary">
                                        Change Password
                                    </button>
                                    <a href="javascript:window.history.back();" class="btn btn-info">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection