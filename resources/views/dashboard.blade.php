@extends('layouts.master')

@section('title', 'Admin Panel')

@section('content')
    <script>
        var pageObject = pageObject || {};
        pageObject.user = {
            name: "{{Auth::user()->email}}",
        }

        $.ajaxSetup({
            beforeSend: function (jqXHR) {
                jqXHR.setRequestHeader('Authorization', 'Bearer ' + "{{Cookie::get('token')}}");
            },
            statusCode: {
                401: function() {
                    window.open("http://milesfair.com:81/auth/logout","_self")
                },
                500: function() {
                    alert("oops, something goes wrong!")
                }
            }
        })
    </script>
    <div id="app">

    </div>
    <script>
        $(document).ready(function(){
            $(document).ajaxStart(function(){
                $("#ajaxLoader").show();
            });
            $(document).ajaxComplete(function(){
                $("#ajaxLoader").hide();
            });
        })
    </script>
    <script src="{{ asset('js/bundle.js') }}"></script>
@endsection