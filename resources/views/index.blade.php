@extends('layouts.master')

@section('title', 'Admin Login')

@section('content')
<div class="container-fluid">
    <div class="center-block" style="margin-top: 10%;">
        <div class="jumbotron" style="width: 600px; max-width: 50%; margin: 0 auto">
            <div class="page-header">
                <h3 class="text-center" style="margin-bottom: 30px">Login To Admin Panel</h3>
                @if(count($errors)>0)
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Error!</strong> These credentials do not match our records.
                    </div>
                @endif
                <form id="frmLogin" class="form-group" method="post" action="{{url('/auth/login')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <input type="email" class="form-control" id="loginEmail" name="email" placeholder="Admin Email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="loginPassword" name="password" placeholder="Password">
                    </div>
                    <div class="form-inline text-center" style="margin-top: 30px">
                        <label>
                            <button type="submit" name="login" class="btn btn-success">Login</button>
                        </label>
                        <br />
                        <a class="pull-right" style="color:orangered; text-decoration: underline" href="{{url('/auth/forget')}}">Forget password?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection