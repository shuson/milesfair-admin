<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>@yield("title")</title>

        <link rel="stylesheet" href="{{ asset('libs/bootstrap-3.3.5/css/bootstrap.min.css') }}" media="screen">
        <link rel="stylesheet" href="{{ asset('libs/bootstrap-3.3.5/css/bootstrap-responsive.min.css') }}" media="screen">
        <link rel="stylesheet" href="{{ asset('libs/font-awesome-4.4.0/css/font-awesome.min.css') }}">

        <!-- style links -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/ajaxLoader.css') }}">

        <script src="{{ asset('libs/jquery-1.11.3.min.js') }}"></script>
        <script src="{{ asset('libs/bootstrap-3.3.5/js/bootstrap.min.js') }}"></script>
    </head>
    <body>
        @yield("content")
    </body>
</html>